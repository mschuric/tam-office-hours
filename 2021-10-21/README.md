# TAM OFFICE HOURS - Oct 21 2021
## Questions

*__What is Fargate pricing?__*

Fargate is priced differently than EKS on EC2. AWS Fargate pricing is calculated based on the vCPU, memory, and storage1 resources used from the time you start to download your container image until the Amazon ECS Task or Amazon EKS2 Pod terminates, rounded up to the nearest second.

### Pricing Details
Pricing is based on requested vCPU, memory, and storage1 resources for the Task or Pod. The three dimensions are independently configurable.
1 Storage resources are currently only available for ECS.

Region: US-EAST-1 (VIRGINIA)
| Component | Price |
------------|-------|
per vCPU per hour |	$0.04048
per GB per hour | $0.004445

https://aws.amazon.com/fargate/pricing/

## Sample Code
You can find the CDK sample code here: 

https://aws.amazon.com/blogs/containers/building-and-deploying-fargate-with-eks-in-an-enterprise-context-using-the-aws-cloud-development-kit-and-cdk8s/

I modified the code sligtly to include the creation  of the VPC. The Git Repo is located here:

https://github.com/aws-samples/cdk-eks-fargate