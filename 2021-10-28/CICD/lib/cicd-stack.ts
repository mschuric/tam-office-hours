import cdk = require('@aws-cdk/core');
import ec2 = require('@aws-cdk/aws-ec2');
import ecr = require('@aws-cdk/aws-ecr');
import eks = require('@aws-cdk/aws-eks');
import iam = require('@aws-cdk/aws-iam');
import codebuild = require('@aws-cdk/aws-codebuild');
import codecommit = require('@aws-cdk/aws-codecommit');
import targets = require('@aws-cdk/aws-events-targets');
import codepipeline = require('@aws-cdk/aws-codepipeline');
import codepipeline_actions = require('@aws-cdk/aws-codepipeline-actions');
import { group } from 'console';
import { SecurityGroup } from '@aws-cdk/aws-ec2';
import { AwsLoadBalancerController } from './loadbalancer-controller';

export class CicdStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'K8sVPC', {
      enableDnsHostnames: true,
      enableDnsSupport: true,
      cidr: '10.0.0.0/16',
      maxAzs: 2,
      subnetConfiguration: [
        { name: 'Private', cidrMask: 18, subnetType: ec2.SubnetType.PRIVATE_WITH_NAT },
        { name: 'Public', cidrMask: 24, subnetType: ec2.SubnetType.PUBLIC }]
    })

    const bastionSSH = new SecurityGroup(this, 'ssh', { vpc: vpc, securityGroupName: 'bastion-ssh' });
    bastionSSH.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(22));

    const userdata = ec2.UserData.forLinux();
    userdata.addCommands(
      'sudo apt-get update',
      'sudo apt-get install -y apt-transport-https ca-certificates curl',
      'sudo curl -LO https://dl.k8s.io/release/v1.22.0/bin/linux/amd64/kubectl',
      'sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl',
      'kubectl version --client'
    )

    new ec2.Instance(this, 'bastion', {
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO),
      machineImage: ec2.MachineImage.latestAmazonLinux(),
      vpc: vpc,
      keyName: 'octank-west',
      vpcSubnets: { subnetGroupName: 'Public' },
      securityGroup: bastionSSH,
      userData: userdata,

    })
    // cluster master role
    const masterRole = new iam.Role(this, 'cluster-master-role', {
      assumedBy: new iam.AccountRootPrincipal(),
      roleName: 'EKSMasterRole'
    });
    masterRole.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonEKSClusterPolicy'));

    const clusterSG = new ec2.SecurityGroup(this, 'ClusterSecurityGroup', { vpc, securityGroupName: 'ClusterSecurityGroup' })
    clusterSG.addIngressRule(ec2.Peer.ipv4('10.0.0.0/16'), ec2.Port.allTraffic())
    clusterSG.addIngressRule(clusterSG, ec2.Port.allTraffic())

    const cluster = new eks.FargateCluster(this, 'my-cluster', {
      version: eks.KubernetesVersion.V1_21,
      mastersRole: masterRole,
      clusterName: 'TAMCluster',
      outputClusterName: true,
      securityGroup: clusterSG,
      endpointAccess: eks.EndpointAccess.PUBLIC, // In Enterprise context, you may want to set it to PRIVATE.
      // kubectlEnvironment: {     // Also if the Enterprise private subnets do not provide implicit internet proxy instead the workloads need to specify https_proxy, then you need to use kubectlEnvironment to set up http_proxy/https_proxy/no_proxy accordingly for the Lambda provissioning EKS cluster behind the scene so that the Lambda can access AWS service APIs via enterprise internet proxy.
      //     https_proxy: "your-enterprise-proxy-server",
      //     http_proxy: "your-enterprise-proxy-server",
      //     no_proxy: "localhost,127.0.0.1,169.254.169.254,.eks.amazonaws.com,websites-should-not-be-accesses-via-proxy-in-your-environment"
      // },
      vpc: vpc,
      vpcSubnets: [{ subnetGroupName: 'Private' }], // you can also specify the subnets by other attributes
    });

    new AwsLoadBalancerController(this, 'loadBalancer', {
      eksCluster: cluster
    })

    const ecrRepo = new ecr.Repository(this, 'EcrRepo');
    const repository = new codecommit.Repository(this, 'CodeCommitRepo', {
      repositoryName: `${this.stackName}-repo`
    });

    const eksRole = new iam.Role(this, 'EKSRole', { roleName: 'EKSRole', assumedBy: new iam.ServicePrincipal('codebuild.amazonaws.com') });
    eksRole.addToPolicy(new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      resources: ['*'],
      actions: [
        "ecr:GetAuthorizationToken",
        "ecr:BatchGetImage",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:PutImage",
        "eks:*",
        "sts:AssumeRole"
      ],
      conditions: [

      ]
    }));

    const project = new codebuild.Project(this, 'MyProject', {
      projectName: `${this.stackName}`,
      role: eksRole,
      source: codebuild.Source.codeCommit({ repository }),
      environment: {
        buildImage: codebuild.LinuxBuildImage.AMAZON_LINUX_2_3,
        privileged: true
      },
      environmentVariables: {
        'CLUSTER_NAME': {
          value: `${cluster.clusterName}`
        },
        'ECR_REPO_URI': {
          value: `${ecrRepo.repositoryUri}`
        }
      },
      buildSpec: codebuild.BuildSpec.fromObject({
        version: "0.2",
        phases: {
          pre_build: {
            commands: [
              'export TAG=${CODEBUILD_RESOLVED_SOURCE_VERSION}',
              'export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output=text)',
              'echo Logging in to Amazon ECR',
              'aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com',
              'env'
            ]
          },
          build: {
            commands: [
              'echo build started',
              // [YOUR DOCKER LOGIN HERE]
              `docker build -t $ECR_REPO_URI:$TAG .`,
              'docker push $ECR_REPO_URI:$TAG'
            ]
          },
          post_build: {
            commands: [
              `aws eks update-kubeconfig --region us-west-2 --name $CLUSTER_NAME --role-arn "${masterRole.roleArn}"`,
              'kubectl get svc',
              'kubectl get deploy -n simple-api',
              'kubectl get svc -n simple-api',
              "isDeployed=$(kubectl get deploy -n simple-api -o json | jq '.items[0]')",
              "deploy8080=$(kubectl get svc -n simple-api -o wide | grep 8080: | tr ' ' '\n' | grep app= | sed 's/app=//g')",
              "echo $isDeployed $deploy8080",
              "if [[ \"$isDeployed\" == \"null\" ]]; then kubectl apply -f k8s/namespace.yaml && kubectl apply -f k8s/serviceBlue.yaml && kubectl apply -f k8s/serviceGreen.yaml; else kubectl set image deployment/$deploy8080 -n simple-api flask=$ECR_REPO_URI:$TAG; fi",
              'kubectl get deploy -n simple-api',
              'kubectl get svc -n simple-api'
            ]
          }
        }
      })
    })
    const project2 = new codebuild.Project(this, 'MyProject2', {
      projectName: `${this.stackName}2`,
      source: codebuild.Source.codeCommit({ repository }),
      role: eksRole,
      environment: {
        buildImage: codebuild.LinuxBuildImage.AMAZON_LINUX_2_3,
        privileged: true
      },
      environmentVariables: {
        'CLUSTER_NAME': {
          value: `${cluster.clusterName}`
        },
        'ECR_REPO_URI': {
          value: `${ecrRepo.repositoryUri}`
        }
      },
      buildSpec: codebuild.BuildSpec.fromObject({
        version: "0.2",
        phases: {
          pre_build: {
            commands: [
              'env',
              'export TAG=${CODEBUILD_RESOLVED_SOURCE_VERSION}',
            ]
          },
          build: {
            commands: [
              'echo "Dummy Action"'
            ]
          },
          post_build: {
            commands: [
              `aws eks update-kubeconfig --region us-west-2 --name $CLUSTER_NAME --role-arn "${masterRole.roleArn}"`,
              'kubectl get svc',
              'kubectl get deploy -n simple-api',
              'kubectl get svc -n simple-api',
              "deploy8080=$(kubectl get svc -n simple-api -o wide | grep ' 8080:' | tr ' ' '\n' | grep app= | sed 's/app=//g')",
              "deploy80=$(kubectl get svc -n simple-api -o wide | grep ' 80:' | tr ' ' '\n' | grep app= | sed 's/app=//g')",
              "echo $deploy80 $deploy8080",
              "kubectl patch svc simple-api-blue -n simple-api -p '{\"spec\":{\"selector\": {\"app\": \"'$deploy8080'\"}}}'",
              "kubectl patch svc simple-api-green -n simple-api -p '{\"spec\":{\"selector\": {\"app\": \"'$deploy80'\"}}}'",
              'kubectl get deploy -n simple-api',
              'kubectl get svc -n simple-api'
            ]
          }
        }
      })
    })

     // PIPELINE

     const sourceOutput = new codepipeline.Artifact();

     const sourceAction = new codepipeline_actions.CodeCommitSourceAction({
       actionName: 'CodeCommit',
       repository,
       output: sourceOutput,
     });
 
     const buildAction = new codepipeline_actions.CodeBuildAction({
       actionName: 'CodeBuild',
       project: project,
       input: sourceOutput,
       outputs: [new codepipeline.Artifact()], // optional
     });
 
 
     const buildAction2 = new codepipeline_actions.CodeBuildAction({
       actionName: 'CodeBuild',
       project: project2,
       input: sourceOutput,
     });
 
 
     const manualApprovalAction = new codepipeline_actions.ManualApprovalAction({
       actionName: 'Approve',
     });
 
 
 
     new codepipeline.Pipeline(this, 'MyPipeline', {
       stages: [
         {
           stageName: 'Source',
           actions: [sourceAction],
         },
         {
           stageName: 'BuildAndDeploy',
           actions: [buildAction],
         },
         {
           stageName: 'ApproveSwapBG',
           actions: [manualApprovalAction],
         },
         {
           stageName: 'SwapBG',
           actions: [buildAction2],
         },
       ],
     });
  }
}