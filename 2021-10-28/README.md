# TAM Office Hours - Oct 28 2021

## Questions
1. 
        Q: Will CloudFormation or CDK allow for visual modeling of infrastructure
        A: Unclear  of the roadmap for either CloudFormation or CDK. Because of the nuances of every environment, this may not be a feasible feature to implement, but I will certainly ask.

2.
        Q: Does EKS/Kubernetes provide the ability to have rolling deployments to avoid downtime?
        A: Yes. Kubectl provides commands for this based on either deployments or services. You can read more about the specifics here:
        https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/

        
