const express = require('express');
const app = express();

app.get('/', (req, res)=>{
    console.log('API request at path "/"');
    return res.json({'hello':'world'});
})

app.get('/user/:id', (req, res) =>{
    const id = req.params.id;
    res.json({id, name: `John doe #${id}`})
})

app.listen(process.env.PORT || 8080, () =>{
    console.log('app is listening...')
})